<?php

namespace GO\Unfilledtimenotifier;
use GO\Base\Module;

class UnfilledtimenotifierModule extends Module {

    public function autoInstall() {
        return true;
    }

    public function author() {
        return '';
    }

    public function authorEmail() {
        return '';
    }

    public function hasInterface() {
        return false;
    }

    public function depends() {
        return ['leavedays'];
    }

}