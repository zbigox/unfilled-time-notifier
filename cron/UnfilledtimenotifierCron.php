<?php
namespace GO\Unfilledtimenotifier\Cron;

use GO;
use GO\Base\Db\PDO;
use GO\Base\Cron\CronJob;
use GO\Base\Cron\AbstractCron;
use GO\Base\Model\User;
use GO\Base\Mail\Message;
use GO\Base\Mail\Mailer;

class UnfilledtimenotifierCron extends AbstractCron {

    public function enableUserAndGroupSupport() {
        return false;
    }

    public function getLabel() {
        return \GO::t('cronName', 'unfilledtimenotifier');
    }

    public function getDescription() {
        return \GO::t('cronDescription', 'unfilledtimenotifier');
    }

    public function run(CronJob $cronJob) {
        $currentMonth = date('m');
        $usersToNotyfy = [];

        foreach($this->getUsers() as $user) {
            $checkingDay = strtotime(date('01-m-Y 00:00:00'));

            while(($checkingDay < time()) && (date('m', $checkingDay) == $currentMonth)) {
                if(!($this->isUserOnVacation($user, $checkingDay) || !$this->isUserWorkingDay($user, $checkingDay)
                    || $this->isUserLoggedTime($user, $checkingDay))) {

                    $usersToNotyfy[$user->id]['user'] = $user;
                    $usersToNotyfy[$user->id]['days'][] = $checkingDay;

                    if(($manager = $this->getUserManager($user, $checkingDay))) {
                        $usersToNotyfy[$user->id]['manager'] = $manager;
                    }
                }

                $checkingDay += 24 * 60 * 60;
            }
        }

        foreach($usersToNotyfy as $user) {
            $this->notyfyUser($user);
        }
    }

    /**
     * @param User $user
     * @param integer $date
     * @return bool
     */
    public function isUserOnVacation(User $user, $date) {
        $q = \GO::getDbConnection()->prepare('SELECT count(id) as count FROM `ld_leave_days` WHERE user_id=:user_id AND status=1 AND '.$date.' BETWEEN first_date AND last_date LIMIT 1');
        $id = $user->id;
        $q->bindParam('user_id', $id, PDO::PARAM_INT);

        $q->execute();
        $data = $q->fetch(PDO::FETCH_OBJ);

        return $data->count > 0;
    }

    /**
     * @param User $user
     * @param integer $checkingDay
     * @return bool
     */
    public function isUserWorkingDay(User $user, $checkingDay) {
        $column = '';

        switch(date('N', $checkingDay)) {
            case 1: $column = 'mo_work_hours'; break;
            case 2: $column = 'tu_work_hours'; break;
            case 3: $column = 'we_work_hours'; break;
            case 4: $column = 'th_work_hours'; break;
            case 5: $column = 'fr_work_hours'; break;
            case 6: $column = 'sa_work_hours'; break;
            case 7: $column = 'su_work_hours'; break;
        }

        $q = \GO::getDbConnection()->prepare("SELECT $column as hours FROM `go_working_weeks` WHERE user_id=:user_id LIMIT 1");
        $id = $user->id;
        $q->bindParam('user_id', $id, PDO::PARAM_INT);

        $q->execute();
        $data = $q->fetch(PDO::FETCH_OBJ);

        return $data ? ($data->hours > 0) : false;
    }

    /**
     * @param User $user
     * @param integer $date
     * @return bool
     */
    public function isUserLoggedTime(User $user, $date) {
        $q = \GO::getDbConnection()->prepare("SELECT id FROM `pr2_hours` WHERE date='$date' AND user_id=:user_id LIMIT 1");
        $id = $user->id;
        $q->bindParam('user_id', $id, PDO::PARAM_INT);

        $q->execute();
        $data = $q->fetch(PDO::FETCH_OBJ);

        return $data ? true : false;
    }

    /**
     * @param User $user
     * @return bool|User[]
     */
    public function getUserManager(User $user, $date) {
        $q = \GO::getDbConnection()->prepare('SELECT manager_user_id FROM `ld_year_credits` WHERE user_id=:user_id AND year='.date('Y', $date).' LIMIT 1');
        $id = $user->id;
        $q->bindParam('user_id', $id, PDO::PARAM_INT);

        $q->execute();
        $data = $q->fetch(PDO::FETCH_OBJ);

        if (!($data && $q->rowCount() > 0)) {
            return false;
        }

        $managerID = $data->manager_user_id;

        $q = \GO::getDbConnection()->prepare('SELECT * FROM `go_users` WHERE id='.$managerID);
        $q->execute();
        $q->setFetchMode(PDO::FETCH_CLASS, "GO\Base\Model\User", array(false));

        if (!$q || $q->rowCount() < 0) {
            return false;
        }

        $data = $q->fetch();

        return $data ? $data : false;
    }

    /**
     * @return User[]
     */
    public function getUsers() {
        $q = \GO::getDbConnection()->prepare('SELECT * FROM `go_users`');
        $q->execute();
        $q->setFetchMode(PDO::FETCH_CLASS, "GO\Base\Model\User", array(false));

        return $q;
    }

    /**
     * @param array $userData
     */
    public function notyfyUser($userData) {
        /**
         * @var User $user
         */
        $user = $userData['user'];
        $days = $userData['days'];

        $activeLang = \GO::language()->getLanguage();
        if ($activeLang !== $user->language) {
            \GO::language()->setLanguage($user->language);
        }

        $content = \GO::t('emailHeading', 'unfilledtimenotifier')."\n\n";

        $content .= "<ul>\n\n";

        foreach($days as $day) {
            $content .= '<li>'.date(join($user->date_separator, str_split($user->date_format)), $day)."</li>\n";
        }

        $content .= "</ul><br /> <br />\n\n";

        $subject = \GO::t('emailSubject', 'unfilledtimenotifier');

        $emailPlaceholders = [
            '[%first_name%]' => $user->first_name,
            '[%last_name%]' => $user->last_name,
        ];

        $subject = str_replace(array_keys($emailPlaceholders), array_values($emailPlaceholders), $subject);
        $content = str_replace(array_keys($emailPlaceholders), array_values($emailPlaceholders), $content);

        $message = Message::newInstance($subject);
        $message->addFrom(\GO::config()->noreply_email, \GO::config()->title);
        $message->addTo($user->email, $user->name);
        $message->setHtmlAlternateBody($content);

        if ($userData['manager']) {
            $message->setCc($userData['manager']->email, $userData['manager']->name);
        }

        Mailer::newGoInstance()->send($message, $failedRecipients);

        if(!empty($failedRecipients))
            trigger_error("Mail send failed for recipient: ".implode(',', $failedRecipients), E_USER_NOTICE);

        if ($activeLang !== $user->language) {
            \GO::language()->setLanguage($activeLang);
        }
    }
}