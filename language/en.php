<?php

$l = [
    'name' => 'Unfilled Time Notifier',
    'description' => '',

    'cronName' => 'Unfilled Time Notifier Cron',
    'cronDescription' => 'It should be run once a month',

    'emailSubject' => '[%first_name%] [%last_name%] not entered the time in this month to the indicated days',
    'emailHeading' => '[%first_name%] [%last_name%] not entered the time in this month on:<br />',
];
