<?php

$l = [
	'name' => 'Unfilled Time Notifier',
	'description' => 'Powiadomienie o niewypełnionym czasie pracy',

	'cronName' => 'Unfilled Time Notifier Cron',
	'cronDescription' => 'Powiadomienia o niewypełnionym czasie pracy. Powinien być uruchamiany raz w miesiącu.',

    'emailSubject' => '[%first_name%] [%last_name%] nie wpisał czasu w tym miesiącu w podanych dniach',
    'emailHeading' => '[%first_name%] [%last_name%] nie wpisał czasu w tym miesiącu w dniach:<br />',
];
